<?php


Route::get('server_info', function() {
ini_set('post_max_size', '10');
	exit(phpinfo());
});

/* Login */
Route::get('login', 'Auth\AuthController@userLogin');

/* Authorization */
Route::post('auth', 'Auth\AuthController@userAuthenticate');

/* Account Creation */
Route::post('create-user', 'WelcomeController@createUser');

/* Forgot Password */
Route::post('forgot-password', 'WelcomeController@forgotPassword');

/* Middleware */
Route::group(['middleware'=>'auth'], function() {
	
	/* Dashboard */
	Route::get('/', 'WelcomeController@index');

	/* Recepients */
	Route::get('recepients/create',			'RecepientsController@setup');
	Route::post('recepients/upload',		'RecepientsController@upload');
	Route::post('recepients/paginate',		'RecepientsController@uploadListPagination');
	Route::post('recepients/manual',		'RecepientsController@manual');
	Route::get('recepients/manage',			'RecepientsController@manage');
	Route::get('recepients/edit/{table}',	'RecepientsController@edit');
	Route::post('recepients/edit/paginate',	'RecepientsController@editListPagination');
	Route::get('recepients/delete/{table}',	'RecepientsController@delete');
	Route::post('recipients/save',			'RecepientsController@save');
	Route::post('recipients/update/{table?}',		'RecepientsController@update');

	/* Marketing */
	Route::get('marketing/create',		'MarketingController@setup');
	Route::get('marketing/manage',		'MarketingController@manage');
	// Route::post('marketing/save',		'MarketingController@save');
	Route::post('marketing/save',			'SendgridController@schedule');
	Route::post('marketing/delete',		'MarketingController@delete');
	Route::get('marketing/edit/{id?}',		'MarketingController@edit');
	Route::get('marketing/reports/{id?}',	'MarketingController@visualizeReport');
	Route::get('metric/{metric?}',	'MarketingController@metricDetails');

	/* Sender Addresses */
	Route::get('sender/manage',			'SenderController@manage');
	Route::get('sender/create',			'SenderController@setup');
	Route::post('sender/save',			'SenderController@save');
	Route::get('sender/edit/{id}',		'SenderController@edit');
	Route::post('sender/update',		'SenderController@update');
	Route::get('sender/delete/{id}',	'SenderController@delete');

	/* SendGrid */
	Route::post('api/send',				'SendgridController@send');
	Route::post('api/send-now',			'SendgridController@sendNow');
	Route::post('api/update-last-sent',	'SendgridController@reloadLastSent');
	Route::post('api/schedule',			'SendgridController@schedule');
	Route::post('api/draft',			'MarketingController@saveAsDraft');

	/* User Accounts */
	Route::get('users', 				'AccountController@index');
	Route::get('users/manage/{id}', 	'AccountController@manageAction');
	Route::get('user/change-password',	'AccountController@changePassword');
	Route::post('user/change-password/confirm',	'AccountController@confirmChangePassword');
	Route::get('user/create',	'AccountController@createAccountUnder');
	Route::post('user/create-under',	'AccountController@createAccountUnderSubmit');

	/* Audit Trail */
	Route::post('audit', 	'AuditController@audit');

	/* Logout */
	Route::get('logout', 	'WelcomeController@logOut');

});

/* SendGrid Event Webhook */
Route::post('event-webhooks', 	'SendgridController@eventWebhook');
Route::get('event-webhooks', 	'SendgridController@eventWebhook');

/* Password Hash Generator */
Route::get('generate-password', function() {
	var_dump(Hash::make('1234'));
});

Route::get('rename', function() {
	$test = '[{"email":"jeffrey.mabazza@nuworks.ph","smtp-id":"<14dd644a46e.3090.36497b@ismtpd0008p1sin1>","timestamp":1433818934,"sg_event_id":"itAPG-O8TWS6bdwYYT27NA","send_at":1433818950,"sg_message_id":"14dd644a46e.3090.36497b.filter0406p1mdw1.7361.557657346.0","category":["Test Category"],"event":"processed"}]';
	echo '<pre>';
	print_r(json_decode($test, TRUE));
	exit;
});

Route::get('hoho', 'SendgridController@eTest');

/* Middleware Controller */
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
