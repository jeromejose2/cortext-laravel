<?php namespace App\Http\Controllers;

use App\Sender;
use App\Audit;
use View;
use Html;
use DB;
use Request;
use Config;

class SenderController extends Controller {

	public function __construct()
	{
	}

	public function setup()
	{
		return View::make('pages.sender-setup');
	}

	public function manage()
	{
		$rows = Sender::paginate(Config::get('constants.pagination.per_page'));
		$total = Sender::count();

		return View::make('pages.sender-manage')->with(compact('rows', 'total'));
	}

	public function save()
	{
		$Sender = new Sender;
		$Audit = new Audit;

		foreach(Request::except('_token') as $k => $v) {
			$Sender->$k = $v;
		}

		$Audit->username = session()->get('user')['username'];
		$Audit->email = session()->get('user')['email'];
		$Audit->activity = 'User '. session()->get('user')['username'] .' has created a new sender';

		$Sender->save();
		$Audit->save();
		return redirect('sender/manage')->with('success', TRUE);
	}

	public function edit($id)
	{
		$row = Sender::find($id);

		return View::make('pages.sender-setup')->with(compact('row'));
	}

	public function update()
	{
		$Sender = Sender::find(Request::input('id'));
		$Audit = new Audit;

		$Audit->username = session()->get('user')['username'];
		$Audit->email = session()->get('user')['email'];
		$Audit->activity = 'User '. session()->get('user')['username'] .' has updated a sender';

		foreach(Request::except('_token') as $k => $v) {
			$Sender->$k = $v;
		}

		$Audit->save();
		$Sender->save();
		return redirect('sender/manage')->with('success', TRUE);
	}

	public function delete($id)
	{
		Sender::find($id)->delete();

		$Audit = new Audit;

		$Audit->username = session()->get('user')['username'];
		$Audit->email = session()->get('user')['email'];
		$Audit->activity = 'User '. session()->get('user')['username'] .' has deleted a sender';

		$Audit->save();

		return redirect('sender/manage');
	}

}