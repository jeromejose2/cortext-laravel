<?php namespace App\Http\Controllers;

use View;
use App\Sender;
use DB;
use Request;
use App\Marketing;

class MarketingController extends Controller {

	public function __construct()
	{
		// $this->middleware('guest');
	}

	public function manage()
	{
		$lists = Marketing::orderBy('id', 'asc')->paginate(15);

		$total = count($lists);
		return View::make('pages.marketing-manage')->with(compact('lists','total'));
	}

	public function setup()
	{
		// var_dump(strtotime("now"));
		// var_dump(date('Y-m-d H:i:s', strtotime('1432796142')));
		// exit;
		$senders = Sender::get();

		$tables = DB::table('recipients_list')->get();

		$lists = [];
		foreach($tables as $k => $v) {
			$lists[$k]['table'] = $v->table_name;
			$lists[$k]['name'] = str_replace('_', ' ', str_replace('client_', '', $v->table_name));
			$lists[$k]['count'] = DB::table('client_'.$v->table_name)->count();
		}

		// $tables = DB::select('SHOW TABLES');

		// $lists = [];
		// foreach($tables as $k => $v) {
		// 	if(strpos($_SERVER['REMOTE_ADDR'] == '192.168.10.1' ? $v->Tables_in_homestead : $v->Tables_in_storytec_cortex, 'client') !== FALSE) {
		// 		$lists[$k]['table'] = $_SERVER['REMOTE_ADDR'] == '192.168.10.1' ? $v->Tables_in_homestead : $v->Tables_in_storytec_cortex;			
		// 		$lists[$k]['name'] = ucwords(str_replace('_', ' ', str_replace('client_', '', $_SERVER['REMOTE_ADDR'] == '192.168.10.1' ? $v->Tables_in_homestead : $v->Tables_in_storytec_cortex)));
		// 		$lists[$k]['count'] = DB::table($_SERVER['REMOTE_ADDR'] == '192.168.10.1' ? $v->Tables_in_homestead : $v->Tables_in_storytec_cortex)->count();
		// 	}
		// }

		return View::make('pages.marketing-setup')->with(compact('senders', 'lists'));
	}

	public function save()
	{
		$marketing = new Marketing;

		foreach(Request::except('_token', 'date', 'time', 'method') as $k => $v) {
			if($k == 'recepients') {
				$v = serialize($v);
			}
			$marketing->$k = $v;
		}

		$marketing->save();

		$marketing = Marketing::find(Request::input('id'));
		$result = $marketing->toArray();
		$sender = Sender::find($result['sender_id']);
		$recepients = unserialize($result['recepients']);
		if($recepients) {
			$sendgrid = new SendGrid(Config::get('constants.sendgrid.api_username'), Config::get('constants.sendgrid.api_password'));
			foreach($recepients as $k => $v) {
				$db_recepients = DB::table($v)->get();

				$email_addresses = array_pluck($db_recepients, 'email_address');

				$email = new SendGrid\Email();
				$email->setSmtpapiTos($email_addresses)
					->setFrom($sender->from_email)
					->setReplyTo($sender->reply_to)
					->setFromName($sender->from_name)
					->setSubject($result['subject'])
					->setHtml($result['html_content']);
					// ->addSubstitution('%name%', ['Jeffrey', 'Jerome', 'Ram']);
				$response = $sendgrid->send($email);
				$marketing->last_sent = date('Y-m-d H:i:s');
				$marketing->save();
			}
		}
	}

	public function saveAsDraft()
	{

		if(Request::input('method') === 'add') {
			$Marketing = new Marketing;

			foreach(Request::except('_token', 'date', 'time', 'method') as $k => $v) {
				if($k == 'recepients') {
					$v = serialize($v);
				}
				$Marketing->$k = $v;
			}

			$Marketing->save();
		} else {
			$Marketing = Marketing::find(Request::input('id'));

			foreach(Request::except('_token', 'date', 'time', 'method', 'is_update') as $k => $v) {
				if($k == 'recepients') {
					$v = serialize($v);
				}
				$Marketing->$k = $v;
			}

			$Marketing->save();
		}
		
	}

	public function edit($id = NULL)
	{
		$marketing = Marketing::find($id);

		if($id === NULL || ! $marketing) {
			return redirect('marketing/manage');
		}

		$senders = Sender::get();

		$tables = DB::table('recipients_list')->get();

		$lists = [];
		foreach($tables as $k => $v) {
			$lists[$k]['table'] = $v->table_name;
			$lists[$k]['name'] = str_replace('_', ' ', str_replace('client_', '', $v->table_name));
			$lists[$k]['count'] = DB::table('client_'.$v->table_name)->count();
		}

		return View::make('pages.marketing-setup')->with(compact('senders', 'lists', 'marketing'));
	}

	public function update()
	{
		
	}

	public function delete()
	{
		$marketing_delete_report = Marketing::find(Request::input('id'));
		$e = DB::table('reports')->where('title', '=', $marketing_delete_report->title)->delete();

		$marketing = Marketing::find(Request::input('id'));
		$marketing->delete();
	}

	public function visualizeReport($id = NULL)
	{
		if($id != NULL) {
			$Marketing = json_decode(json_encode(DB::table('marketings')->where('id', '=', $id)->get()), TRUE);
			
			// $Marketing = Marketing::find($id)->get()->toArray();
			if($Marketing) {
				$title = $Marketing[0]['title'];
				$final_graph = [];
				$total_email = DB::table('reports')->select('email')->where('title', '=', $Marketing[0]['title'])->distinct('email')->get();
				$total = count($total_email);
				$graph = json_decode(json_encode(DB::table('reports')->where('title', '=', $Marketing[0]['title'])->select('title', 'event', DB::raw('count(distinct(`email`)) as total'))->groupBy('title')->groupBy('event')->get()), TRUE);
				
				foreach($graph as $k => $v) {
					$percent = $v['total'] / $total;
					$final_graph[$k]['title'] = $v['title'];
					$final_graph[$k]['event'] = $v['event'];
					$final_graph[$k]['total'] = $v['total'];
					$final_graph[$k]['percent'] = $percent;
					$final_graph[$k]['percent_friendly'] = number_format($percent * 100, 2);
				}
				// $final_graph[] = ['title'=>$Marketing[0]['title'], 'event'=>'total', 'total'=>count($total_email)];

				

				return View::make('pages.marketing-reports')->with(compact('final_graph', 'title', 'total'));
			} else {
				return redirect('marketing/manage');
			}
		} else {
			return redirect('marketing/manage');
		}
	}

	public function metricDetails($metric)
	{
		$total = count(json_decode(json_encode(DB::table('reports')->distinct()->select('email', 'title')->where('event', '=', $metric)->get()), TRUE));
		if($metric == 'total') {
			// $results = DB::table('reports')->groupBy('email')->groupBy('title')->groupBy('event')->where(function($query) {
			// 	$overall_recipients = json_decode(json_encode(DB::table('marketings')->where('last_sent', '<', date('Y-m-d H:i:s'))->where('last_sent', '!=', '0000-00-00 00:00:00')->get()),TRUE);
			// 	$recipients_count = array_fetch($overall_recipients, 'title');
			// 	if($recipients_count) {
			// 		foreach($recipients_count as $k) {
			// 			$query->orWhere('title', '=', $k);
			// 		}
			// 	}
			// })->paginate(15);

			$results = DB::table('reports')->distinct()->groupBy('email')->groupBy('title')->paginate(15);
			$total = count(json_decode(json_encode(DB::table('reports')->distinct()->groupBy('email')->groupBy('title')->get()), TRUE));
			// echo '<pre>';
			// print_r($results);
			// exit;
			// $results = DB::table('reports')->select('email', 'title')->paginate(15);
		}else if($metric == 'open') {
			$results = DB::table('reports')->groupBy('email')->groupBy('title')->select('email', 'title')->where('event', '=', $metric)->paginate(15);
			$total = count(json_decode(json_encode(DB::table('reports')->groupBy('email')->groupBy('title')->select('email', 'title')->where('event', '=', $metric)->get()), TRUE));
		} else {
			$results = DB::table('reports')->select('email', 'title')->where('event', '=', $metric)->paginate(15);
		}

		return View::make('pages.metric-details')->with(compact('results', 'total'));
	}

}