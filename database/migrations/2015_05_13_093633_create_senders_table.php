<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('senders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('from_name', 100);
			$table->string('from_email', 100);
			$table->string('reply_to', 100);
			$table->text('description');
			$table->text('address');
			$table->string('city', 100);
			$table->string('state', 50)->nullable();
			$table->string('zip', 10)->nullable();
			$table->string('country', 50);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('senders');
	}

}
