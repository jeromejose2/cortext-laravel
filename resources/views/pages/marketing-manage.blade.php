@extends('layouts.default')
@section('styles')
	<link href="{{ asset('assets/global/plugins/icheck/skins/all.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/global/plugins/select2/select2.css') }}" rel="stylesheet" type="text/css">
@stop

@section('content')
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<span>Home</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa icon-envelope-open"></i>
						<span>Campaign</span>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa icon-user"></i>
						<span>Manage</span>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box yellow-lemon">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-user"></i>Campaign <span class="badge badge-alert">{{ $total }} </span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-responsive">
								<table class="table table-bordered">
								<thead>
								<tr>
									<th>
										 #
									</th>
									<th>
										 Marketing Email Title
									</th>
									<th>
										 Number of Recepients
									</th>
									<th>
										Status
									</th>
									<th>
										 Action
									</th>
								</tr>
								</thead>
								<tbody>
								@if($lists)
									<?php $offset = isset($_GET['page']) && $_GET['page'] && $_GET['page'] != 1 ? $_GET['page'] * 15 - 15 : NULL ?>
									@foreach($lists as $k => $v)
										<tr id="row-{{ $v['id'] }}">
											<td>
												<?php $offset = ($offset === NULL ? $k : $offset) + 1 ?>
												{{ $offset }}
											</td>
											<td>
												{{ $v['title'] }}
											</td>
											<td>
												<?php $count = 0; ?>
												<?php $recipients = unserialize($v['recepients']) ?>
												<?php foreach($recipients as $k): ?>
													<?php $count = $count + DB::table('client_'.$k)->count() ?>
												<?php endforeach; ?>
												<?php echo $count ?>
											</td>
											<td>
												<div id="last_sent-{{ $v['id'] }}">
													@if($v['last_sent'] != '0000-00-00 00:00:00')
														@if(date('Y-m-d H:i:s') < date('Y-m-d H:i:s', strtotime($v['last_sent'])))
															{{ date('M d, Y h:i:s A', strtotime($v['last_sent'])) }}
															<span class="badge badge-primary badge-roundless">Scheduled</span>
														@else
															<span class="badge badge-primary badge-roundless">Sent</span> @ {{ date('M d, Y h:i:s A', strtotime($v['last_sent'])) }}
														@endif
													@else
														@if($v['schedule_options'] == 'draft')
															<span class="badge badge-primary badge-roundless">Draft</span>
														@endif
													@endif
													{{-- {{ $v['last_sent'] != '0000-00-00 00:00:00' ? date('M d, Y h:i:s A', strtotime($v['last_sent'])) : '' }} --}}
												</div>
											</td>
											<td>
												@if($v['last_sent'] == '0000-00-00 00:00:00')
													<a id="edit-btn-{{ $v['id'] }}" class="btn default btn-xs green" href="#confirm-{{ $v['id'] }}" role="button" data-toggle="modal">
													<i class="fa fa-edit"></i> Edit </a>
												@endif												
												<a href="#delete-{{ $v['id'] }}" class="btn default btn-xs red" role="button" data-toggle="modal">
												<i class="fa fa-trash-o"></i> Remove </a>

												@if($v['last_sent'] != '0000-00-00 00:00:00' && $v['schedule_options'] != 'send_later')
													@if(date('Y-m-d H:i:s') < date('Y-m-d H:i:s', strtotime($v['last_sent'])))
														<a id="send-btn-{{ $v['id'] }}" href="#send-{{ $v['id'] }}" class="btn default btn-xs blue" role="button" data-toggle="modal">
														<i class="fa fa-mail-forward"></i> Send Now </a>
													@endif
												@elseif($v['last_sent'] == '0000-00-00 00:00:00')
													<a id="send-btn-{{ $v['id'] }}" href="#send-{{ $v['id'] }}" class="btn default btn-xs blue" role="button" data-toggle="modal">
													<i class="fa fa-mail-forward"></i> Send Now </a>
												@endif

												{{-- @if($v['last_sent'] == '0000-00-00 00:00:00' || $v['last_sent'] $v['schedule_options'] == 'draft' || $v['created_at'] == '0000-00-00 00:00:00') --}}
													{{-- <a href="#send-{{ $v['id'] }}" class="btn default btn-xs blue" role="button" data-toggle="modal"> --}}
													{{-- <i class="fa fa-mail-forward"></i> Send Now </a> --}}
												{{-- @endif --}}
												<a href="#report-{{ $v['id'] }}" class="btn default btn-xs grey-cascade" role="button" data-toggle="modal">
												<i class="fa fa-trash-o"></i> Report </a>
											</td>

										</tr>


										{{-- Confirmation Modal --}}
										<div id="confirm-{{ $v['id'] }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Would you like to continue <strong>EDITING</strong> this item?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default" onclick="doThis({{ $v['id'] }}, 'edit')" data-id="{{ $v['id'] }}">Yes</button>
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														{{-- <a class="btn green" href="{{ url('recepients/edit') .'/'. $v['table'] }}">Continue</a> --}}
													</div>
												</div>
											</div>
										</div>

										<div id="report-{{ $v['id'] }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Would you like to continue <strong>VIEWING THE REPORT</strong> of this item?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														<a class="btn green" href="{{ url('marketing/reports') .'/'. $v['id'] }}">Continue</a>
													</div>
												</div>
											</div>
										</div>

										<div id="delete-{{ $v['id'] }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Would you like to continue <strong>REMOVING</strong> this item?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default" onclick="doThis({{ $v['id'] }}, 'delete')">Yes</button>
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														{{-- <a class="btn green" href="{{ url('recepients/delete') . '/' . $v['table'] }}">Continue</a> --}}
													</div>
												</div>
											</div>
										</div>

										<div id="send-{{ $v['id'] }}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
														<h4 class="modal-title">Confirmation</h4>
													</div>
													<div class="modal-body">
														<p>
															 Would you like to continue <strong>SENDING</strong> action?
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" data-dismiss="modal" class="btn default" onclick="doThis({{ $v['id'] }}, 'send')">Yes</button>
														<button type="button" data-dismiss="modal" class="btn default">Cancel</button>
														{{-- <a class="btn green" href="{{ url('recepients/delete') . '/' . $v['table'] }}">Continue</a> --}}
													</div>
												</div>
											</div>
										</div>
										{{-- Confirmation Modal --}}

									@endforeach
								@else
									<tr>
										<td colspan="10" style="text-align: center">No Record(s) Found.</td>
									</tr>
								@endif

								
								</tbody>
								</table>
								<div class="dataTables_paginate paging_simple_numbers">
									{!! $lists->render() !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

	<div class="modal fade" id="ajax" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<img src="../../assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
					<span>
					&nbsp;&nbsp;Sending... </span>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="ajax-delete" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<img src="../../assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
					<span>
					&nbsp;&nbsp;Deleting... </span>
				</div>
			</div>
		</div>
	</div>
@stop

@section('plugins')
	<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/global/plugins/icheck/icheck.min.js') }}" type="text/javascript"></script>
@stop
@section('scripts')
	<script src="{{ asset('assets/global/scripts/metronic.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/admin/pages/scripts/form-wizard.js') }}" type="text/javascript"></script>
	<script>
		function doThis(id, action, mid) {
			var token = "{!! csrf_token() !!}";
			switch(action) {
				case 'edit':
					var url = "{{ url('marketing/edit') }}" + "/" + id;
					jQuery('#ajax').modal('show');
					window.location.href = url;
					break;
				case 'send':
					jQuery('#ajax').modal('show');
					jQuery.post("{{ url('api/send-now') }}", {id:id, _token:token}, function() {
						jQuery('#ajax').modal('hide');
						jQuery('#last_sent-'+id).html('<img src="../../assets/global/img/loading-spinner-grey.gif" alt="" class="loading">');
						jQuery.post("{{ url('api/update-last-sent') }}", {id:id, _token:'{!! csrf_token() !!}'}, function(response) {
							jQuery('#last_sent-'+id).html('<span class="badge badge-primary badge-roundless">Sent</span> @ '+response);
							jQuery('#send-btn-'+id).hide();
							jQuery('#edit-btn-'+id).hide();
						});
					});
					break;
				case 'delete':
					jQuery('#ajax-delete').modal('show');
					jQuery.post("{{ url('marketing/delete') }}", {id:id, _token:token}, function() {
						jQuery('#ajax-delete').modal('hide');
						jQuery('#row-'+id).hide('slow');
					});
					break;
				default:
					break;
			}
		}
	</script>
@stop
@section('defined-scripts')
	<script>
	jQuery(document).ready(function() {       
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Demo.init();
		FormWizard.init();
	});
	</script>
@stop